package cs108;

import java.util.Objects;

import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.scene.image.Image;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;

public final class Mandelbrot {
    private static final Parameters INITIAL_PARAMETERS =
            new Parameters(500, new Point(-0.5, 0), 3.4, 300, 250);

    private final IntegerProperty maxIterationsProperty;

    private final ObjectProperty<Point> frameCenterProperty;
    private final DoubleProperty frameWidthProperty;

    private final IntegerProperty widthProperty;
    private final IntegerProperty heightProperty;

    private final ObjectProperty<Parameters> parametersProperty;
    private final ObjectProperty<Image> imageProperty;

    public Mandelbrot(int maxIterations) {
        this.maxIterationsProperty = new SimpleIntegerProperty(maxIterations);
        this.frameCenterProperty = new SimpleObjectProperty<>(INITIAL_PARAMETERS.frameCenter);
        this.frameWidthProperty = new SimpleDoubleProperty(INITIAL_PARAMETERS.frameWidth);
        this.widthProperty = new SimpleIntegerProperty(INITIAL_PARAMETERS.width);
        this.heightProperty = new SimpleIntegerProperty(INITIAL_PARAMETERS.height);
        this.parametersProperty = new SimpleObjectProperty<>(INITIAL_PARAMETERS);
        this.imageProperty = new SimpleObjectProperty<>(computeImage(INITIAL_PARAMETERS));

        ChangeListener<Object> parameterChangeListener = (p, o, n) -> {
            Platform.runLater(() -> parametersProperty.set(getParameters()));
        };
        maxIterationsProperty.addListener(parameterChangeListener);
        frameCenterProperty.addListener(parameterChangeListener);
        frameWidthProperty.addListener(parameterChangeListener);
        widthProperty.addListener(parameterChangeListener);
        heightProperty.addListener(parameterChangeListener);

        parametersProperty.addListener((p, o, n) -> imageProperty.set(computeImage(getParameters())));
    }

    public IntegerProperty maxIterationsProperty() { return maxIterationsProperty; }
    public int getMaxIterations() { return maxIterationsProperty.get(); }
    public void setMaxIterations(int newMaxIterations) { maxIterationsProperty.set(newMaxIterations); }

    public ObjectProperty<Point> frameCenterProperty() { return frameCenterProperty; }
    public Point getFrameCenter() { return frameCenterProperty.get(); }
    public void setFrameCenter(Point newFrameCenter) { frameCenterProperty.set(newFrameCenter); }

    public DoubleProperty frameWidthProperty() { return frameWidthProperty; }
    public double getFrameWidth() { return frameWidthProperty.get(); }
    public void setFrameWidth(double newFrameWidth) { frameWidthProperty.set(newFrameWidth); }

    public Rectangle getFrame() {
        return Parameters.frameFor(getWidth(), getHeight(), getFrameCenter(), getFrameWidth());
    }

    public IntegerProperty widthProperty() { return widthProperty; }
    public int getWidth() { return widthProperty.get(); }
    public void setWidth(int newWidth) { widthProperty.set(newWidth); }

    public IntegerProperty heightProperty() { return heightProperty; }
    public int getHeight() { return heightProperty.get(); }
    public void setHeight(int newHeight) { heightProperty.set(newHeight); }

    public ReadOnlyObjectProperty<Image> imageProperty() { return imageProperty; }
    public Image getImage() { return imageProperty.get(); }

    private Parameters getParameters() {
        return new Parameters(getMaxIterations(), getFrameCenter(), getFrameWidth(), getWidth(), getHeight());
    }

    private final static class Parameters {
        private final int maxIterations;
        private final Point frameCenter;
        private final double frameWidth;
        private final int width, height;

        public static Rectangle frameFor(int width, int height, Point frameCenter, double frameWidth) {
            double frameHeight = frameWidth * ((double)(height - 1) / (double)(width - 1));
            return Rectangle.ofCenterAndSize(frameCenter, frameWidth, frameHeight);
        }

        public Parameters(int maxIterations, Point frameCenter, double frameWidth, int width, int height) {
            this.maxIterations = maxIterations;
            this.frameCenter = frameCenter;
            this.frameWidth = frameWidth;
            this.width = width;
            this.height = height;
        }

        public Rectangle frame() {
            return frameFor(width, height, frameCenter, frameWidth);
        }

        @Override
        public boolean equals(Object thatO) {
            return (thatO instanceof Parameters)
                    && (maxIterations == ((Parameters)thatO).maxIterations)
                    && (frameCenter.equals(((Parameters)thatO).frameCenter))
                    && (frameWidth == ((Parameters)thatO).frameWidth)
                    && (width == ((Parameters)thatO).width)
                    && (height == ((Parameters)thatO).height);
        }

        @Override
        public int hashCode() {
            return Objects.hash(maxIterations, frameCenter, frameWidth, width, height);
        }
    }

    private static Image computeImage(Parameters p) {
        WritableImage image = new WritableImage(p.width, p.height);
        PixelWriter pixWriter = image.getPixelWriter();

        Rectangle frame = p.frame();
        double delta = frame.width() / (p.width - 1);
        for (int x = 0; x < p.width; ++x) {
            for (int y = 0; y < p.height; ++y) {
                double cR = frame.minX() + delta * x, cI = frame.maxY() - delta * y;
                double zr = cR, zi = cI;
                int i = 1;
                while (zr * zr + zi * zi < 4d && i < p.maxIterations) {
                    double zr1 = zr * zr - zi * zi + cR;
                    double zi1 = 2d * zr * zi + cI;
                    zr = zr1;
                    zi = zi1;
                    i += 1;
                }

                double q = 1d - Math.pow((double)i / p.maxIterations, 0.25);
                int pI = (int)(q * 255.9999);
                int g = 0xFF000000 | (pI << 16) | (pI << 8) | pI;

                pixWriter.setArgb(x, y, g);
            }
        }
        return image;
    }
}
